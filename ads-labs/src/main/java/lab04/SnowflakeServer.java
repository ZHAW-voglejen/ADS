package lab04;

import main.CommandExecutor;

public class SnowflakeServer implements CommandExecutor {
    Turtle turtle;

    private void snowflake(int level, double dist) {
        if (level == 0) {
            turtle.move(dist);
        } else {
            level--;
            dist = dist / 3;
            snowflake(level, dist);
            turtle.turn(60);
            snowflake(level, dist);
            turtle.turn(-120);
            snowflake(level, dist);
            turtle.turn(60);
            snowflake(level, dist);
        }
    }

    public String execute(String command) {
        turtle = new Turtle(0.1, 0.75);

        for (int i = 0; i < 3; i++) {
            snowflake(Integer.parseInt(command), 0.8);
            turtle.turn(-120);
        }

        return turtle.getTrace();
    }
}