package lab04;

import main.CommandExecutor;

public class HanoiServer implements CommandExecutor {

    private StringBuilder output;

    private void writeLn(String line) {
        output.append(line).append('\n');
    }

    public String execute(String command) {
        output = new StringBuilder();

        int n = 3;

        hanoi('A', 'B', 'C', n);

        return output.toString();
    }

    public String run(int n) {
        output = new StringBuilder();
        hanoi('A', 'B', 'C', n);
        return output.toString();
    }

    /**
     * Bewegt n Scheiben von Turm a nach Turm c und benutzt als
     * Zwischenspeicher Turm b.
     */
    private void hanoi(char from, char to, char help, int n)
    {
        if (n > 0) {
            hanoi(from, help, to, n - 1);
            writeLn("Move " + from + " -> " + to);
            hanoi(help, to, from, n - 1);
        }
    }
}
