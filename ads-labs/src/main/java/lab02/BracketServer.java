package lab02;

import ch.voglejen.ads.p1.BracesMatcher;
import main.CommandExecutor;

public class BracketServer implements CommandExecutor {

    private final BracesMatcher matcher;

    public BracketServer() {
        matcher = new BracesMatcher();
        matcher.addBrace("[", "]");
        matcher.addBrace("{", "}");
        matcher.addBrace("<", ">");
        matcher.addBrace("(", ")");
        matcher.addBrace("<*", "*>");
    }

    public String execute(String inputString) {

        if (checkBrackets(inputString))
        {
            return "okay\n";
        }
        else
        {
            return "not okay\n";
        }
    }

    public boolean checkBrackets (String arg)
    {
        return matcher.checkStr(arg);
    }

}
