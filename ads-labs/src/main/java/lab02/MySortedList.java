package lab02;

import java.util.ArrayList;
import java.util.Collections;

public class MySortedList<T extends Comparable> extends MyList<T> {

    @SuppressWarnings("unchecked")
	@Override
    public boolean add(T val) {
        boolean result = super.add(val);
        Collections.sort(this);
        return result;
    }
}
