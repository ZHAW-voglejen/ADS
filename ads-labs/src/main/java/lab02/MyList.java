package lab02;

import java.util.*;

class ListNode<T> {
    Comparable<T> value;
    ListNode<T> next, prev;

    @SuppressWarnings("unchecked")
	ListNode(T value) {
        this.value = (Comparable<T>) value;
    }
}

public class MyList<T> extends AbstractList<T> {
    protected int size;
    protected final ListNode<T> HEADER; // dummy Element

    protected void insertBefore(T val, ListNode<T> p) {
        ListNode<T> n = new ListNode<>(val);
        n.next = p;
        n.prev = p.prev;
        if (p.prev != null) {
            p.prev.next = n;
        }
        p.prev = n;
        size++;
    }

    protected void remove(ListNode<T> p) {
        if (p.prev != null) {
            p.prev.next = p.next;
        }
        p.next.prev = p.prev;
        size--;
    }
    
    // ============== public ===================
    public MyList() {
        HEADER = new ListNode<>(null);
        clear();
    }

    @Override
    public void clear() {
        HEADER.prev = null;
        size = 0;
    }

    @Override
    public boolean add(T val) {
        insertBefore(val, HEADER);
        return true;
    }

    @Override
    public boolean remove(Object val) {
        for (int i = 0; i < size; i++) {
            ListNode<T> node = getNode(i);

            if (Objects.equals(node.value, val)) {
                remove(node);
                return true;
            }

        }
        return false;
    }

    protected ListNode<T> findNode(Object value) {
        ListNode<T> element = HEADER;
        while (element != null) {
            if (value.equals(element.value)) {
                return element;
            }

            element = element.prev;
        }

        return null;
    }

    protected ListNode<T> getNode(int pos) {
        ListNode<T> element = HEADER.prev;
        for (int i = 0; i < size - pos - 1; i++) {
            if (element == null) {
                throw new IndexOutOfBoundsException(pos);
            }

            element = element.prev;
        }

        if (element != null) {
            return element;
        }

        throw new IndexOutOfBoundsException(pos);
    }

    @Override
    public T get(int pos) {
        ListNode<T> node = getNode(pos);
        if (node != null) {
            return (T) node.value;
        }

        throw new IndexOutOfBoundsException(pos);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size != 0;
    }

    @Override
    public T set(int index, T element) {
        ListNode<T> node = getNode(index);
        T oldValue = (T) node.value;
        node.value = (Comparable<T>) element;
        return oldValue;
    }
}
