package lab13;

import org.junit.Test;
import org.junit.Before;
import org.junit.*;
import org.junit.runners.*;
import static org.junit.Assert.*;
import java.util.*;


public class HeapTest {
    private CObject new_CObject(Object s) {
        CObject obj = (CObject) Storage._new("CObject", s);
        return obj;
    }

    static CObject a, d;
    CObject b, c, e, f, g;

    @Before
    public void setUp() throws Exception {}

    private void testContent(String message, Iterable<Collectable> content, String expected) {
        StringBuilder b = new StringBuilder();
        for (Object o  : content) {
            System.out.println(o);
            b.append(o.toString());
        }
        assertEquals(message, expected, b.toString());
    }

    void newObjects() {
        a = new_CObject("A");
        b = new_CObject("B");
        c = new_CObject("C");
        d = new_CObject("D");
        e = new_CObject("E");
        f = new_CObject("F");
        g = new_CObject("G");
        Storage.addRoot(a);
        Storage.addRoot(d);
        a.next = b;
        b.next = c;
        b.down = a;
        c.down = d;
        d.next = e;
        e.next = f;
        f.next = g;
    }

    @Test
    public void testSimple() {
        Storage.clear();
        newObjects();
        testContent("ROOT", Storage.getRoot(), "AD");
        testContent("HEAP1", Storage.getHeap(), "ABCDEFG");
        Storage.gc();
        testContent("HEAP2", Storage.getHeap(), "ABCDEFG");
        e.next = d;
        Storage.gc();
        testContent("HEAP3", Storage.getHeap(), "ABCDE");
        a.next = null;
        Storage.gc();
        testContent("HEAP4", Storage.getHeap(), "ADE");
        Storage.gc();
        testContent("HEAP5", Storage.getHeap(), "ADE");
    }

    @Test
    public void testWithFinalizer() {
        Storage.clear();
        newObjects();
        testContent("ROOT", Storage.getRoot(), "AD");
        testContent("HEAP1", Storage.getHeap(), "ABCDEFG");
        Storage.gc();
        testContent("HEAP2", Storage.getHeap(), "ABCDEFG");
        e.next = d;
        Storage.gc();
        testContent("HEAP3", Storage.getHeap(), "ABCDEFG");
        a.next = null;
        Storage.gc();
        testContent("HEAP4", Storage.getHeap(), "ABCDE");
        Storage.gc();
        testContent("HEAP5", Storage.getHeap(), "ADE");
    }

}