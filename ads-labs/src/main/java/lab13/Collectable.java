package lab13;

public interface Collectable {
    public void setMark(boolean b);
    public boolean isMarked();
    public void finalize();
}
