package lab01;

import java.util.regex.*;

import ch.voglejen.ads.p1.XMLChecker;
import main.CommandExecutor;

public class WellFormedXmlServer implements CommandExecutor {


    public String execute(String command) {
        XMLChecker checker = new XMLChecker(command);
        if (checker.checkWellFormed()) {
            return "ok\n";

        } else {
            return "not ok\n";
        }
    }
}
