/**
 * AnyServer -- Praktikum Experimentierkasten -- ADS
 *
 * @author E. Mumprecht
 * @version 1.0 -- Geruest fuer irgendeinen Server
 */
package lab01;

import ch.voglejen.ads.p1.Divisionable;
import main.CommandExecutor;

public class KGVServer implements CommandExecutor {

    //----- Dies implementiert das CommandExecutor Interface.
    @Override
    public String execute(String command) {

        return kgv(command);
    }

    private String kgv(String s)
    {
        String[] input = s.split(" ");
        int a = Integer.parseInt(input[0]);
        int b = Integer.parseInt(input[1]);

        return "" + kgv(a, b);
    }

    public int kgv(int a, int b)
    {
        return Divisionable.kgV(a, b);
    }

    private static int ggt(int number1, int number2) {
    	return Divisionable.ggT(number1, number2);
    }
}
