package lab01;

import ch.voglejen.ads.p1.BracesMatcher;
import main.CommandExecutor;

public class BracketServer implements CommandExecutor {

    public String execute(String inputString) {

        if (checkBrackets(inputString))
        {
            return "okay\n";
        }
        else
        {
            return "not okay\n";
        }
    }

    public boolean checkBrackets (String arg)
    {
        BracesMatcher matcher = new BracesMatcher();
        matcher.addBrace('(', ')');
        matcher.addBrace('{', '}');
        matcher.addBrace('[', ']');

        return matcher.checkStr(arg);
    }
}
