package lab05;

import java.util.ArrayDeque;
import java.util.Queue;

public class TreeTraversal<T extends Comparable<T>> implements Traversal<T> {

    private TreeNode<T> root;

    public TreeTraversal(TreeNode<T> root) {
        this.root = root;
    }

    public void inorder(Visitor<T> visitor) {
        if (root == null) {
            return;
        }

        inorder_recursive(root, visitor);
    }

    private void inorder_recursive(TreeNode<T> currentNode, Visitor<T> vis) {
        if (currentNode.left != null) {
            inorder_recursive(currentNode.left, vis);
        }

        vis.visit(currentNode.element);

        if (currentNode.right != null) {
            inorder_recursive(currentNode.right, vis);
        }
    }

    public void preorder(Visitor<T> visitor) {
        if (root == null) {
            return;
        }

        preorder_recursive(root, visitor);
    }

    private void preorder_recursive(TreeNode<T> currentNode, Visitor<T> vis) {
        vis.visit(currentNode.element);
        if (currentNode.left != null) {
            preorder_recursive(currentNode.left, vis);
        }

        if (currentNode.right != null) {
            preorder_recursive(currentNode.right, vis);
        }
    }

    public void postorder(Visitor<T> visitor) {
        if (root == null) {
            return;
        }

        postorder_recursive(root, visitor);
    }

    private void postorder_recursive(TreeNode<T> currentNode, Visitor<T> vis) {
        if (currentNode.left != null) {
            postorder_recursive(currentNode.left, vis);
        }

        if (currentNode.right != null) {
            postorder_recursive(currentNode.right, vis);
        }
        vis.visit(currentNode.element);
    }


    @Override
    public void levelorder(Visitor<T> visitor) {
        if (root == null) {
            return;
        }

        Queue<TreeNode<T>> queue = new ArrayDeque<>();
        queue.add(root);
        TreeNode<T> currentNode;

        while (!queue.isEmpty()) {
            currentNode = queue.remove();
            visitor.visit(currentNode.element);
            if (currentNode.left != null) {
                queue.add(currentNode.left);
            }

            if (currentNode.right != null) {
                queue.add(currentNode.right);
            }
        }

    }

    @Override
    public void interval(Comparable<T> min, Comparable<T> max, Visitor<T> visitor) {
        interval(root, min, max, visitor);
    }

    private void interval(TreeNode<T> current, Comparable<T> min, Comparable<T> max, Visitor<T> visitor) {
        if (current == null) {
            return;
        }

        int minCompare = min.compareTo(current.element);
        int maxCompare = max.compareTo(current.element);

        if (minCompare < 0) {
            interval(current.left, min, max, visitor);
        }

        if (minCompare <= 0 && maxCompare >= 0) {
            visitor.visit(current.element);
        }

        if (maxCompare > 0) {
            interval(current.right, min, max, visitor);
        }
    }
}
