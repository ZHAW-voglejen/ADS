package lab03;

import main.CommandExecutor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class RankingServer implements CommandExecutor {
    private static final String SEPARATING_CHAR = ";";

    @Override
    public String execute(String command) throws Exception {

        List<Competitor> competitors = parseCSV(command);

        String ranking = getRanking(competitors);
        String nameList = getNameList(competitors);

        return Competitor.getHeader() + "\n" + ranking + "\n\n-----------\n\n" + Competitor.getHeader() + "\n" + nameList;
    }

    private String getRanking(List<Competitor> competitors) {
        Competitor.sortByRanks(competitors);
        StringBuilder sb = new StringBuilder();
        for (Competitor competitor : competitors) {
            sb.append(competitor.toString()).append("\n");
        }

        return sb.toString();
    }

    private String getNameList(List<Competitor> competitors) {
        Competitor.sortByName(competitors);
        StringBuilder sb = new StringBuilder();
        for (Competitor competitor : competitors) {
            sb.append(competitor.toString()).append("\n");
        }

        return sb.toString();
    }


    private List<Competitor> parseCSV(String csv) throws IOException, ParseException {
        ArrayList<Competitor> list = new ArrayList<>();

        CSVParser parser = CSVFormat.newFormat(';').parse(new StringReader(csv));
        for (CSVRecord record : parser.getRecords()) {
            list.add(new Competitor(
                    Integer.parseInt(record.get(0)),
                    record.get(1),
                    Integer.parseInt(record.get(2)),
                    record.get(3),
                    record.get(4)
            ));
        }

        return list;
    }
}
