package lab03;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Competitor implements Comparable<Competitor> {
    private String name;
    private String country;
    private long time;
    private int jg;
    private int startNr;
    private int rank;

    public static final Comparator<Competitor> NAME_COMPARATOR = (o1, o2) -> {
        return o1.name.compareTo(o2.name);
    };

    public Competitor(int startNr, String name, int jg, String country, String time) throws ParseException {
        this.startNr = startNr;
        this.name = name;
        this.jg = jg;
        this.country = country;
        this.time = parseTime(time);
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public String getName() {
        return name;
    }

    public int getJg() {
        return jg;
    }

    private static long parseTime(String s) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.S");
        return df.parse(s).getTime();
    }

    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.S");
        return rank + "\t" +
                name + "\t" +
                jg + "\t" +
                df.format(new Date(time));
    }

    public static String getHeader() {
        return "Rank\tName\tYear\tTime";
    }

	@Override
	public int compareTo(Competitor o) {
        return Comparator.comparingLong(Competitor::getTime).compare(this, o);
    }

    public static void sortByRanks(List<Competitor> competitors) {
        Collections.sort(competitors);

        int i = 1;
        for (Competitor competitor : competitors) {
            competitor.rank = i;
            i++;
        }
    }

    public static void sortByName(List<Competitor> competitors) {
        competitors.sort((o1, o2) -> {
            int result = o1.getName().compareTo(o2.getName());
            if (result == 0) {
                return Comparator.comparing(Competitor::getJg).compare(o1, o2);
            }
            return result;
        });
    }

}
