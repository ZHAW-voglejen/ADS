package ch.voglejen.ads.p1;

import java.lang.reflect.Array;

public class Stack<T> {

    private int currentPosition = 0;

    private T[] list;

    public Stack(Class<T> type, int size) {
        list = (T[]) Array.newInstance(type, size);
    }

    public void push(T obj) {
        list[currentPosition] = obj;
        currentPosition++;
    }

    public T pop() {
        currentPosition--;
        T obj = list[currentPosition];
        list[currentPosition] = null;
        return obj;
    }

    public int size() {
        return currentPosition;
    }


}
