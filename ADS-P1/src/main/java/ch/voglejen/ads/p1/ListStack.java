package ch.voglejen.ads.p1;

import java.util.ArrayList;
import java.util.List;

public class ListStack<T> {

    private int index;
    private List<T> list;

    public ListStack() {
        list = new ArrayList<>();
    }

    public void push(T item) {
        list.add(item);
    }

    public T pop() {
        if (list.size() == 0) {
            return null;
        }

        return list.remove(list.size() - 1);
    }

    public T peek() {
        if (isEmpty()) {
            return null;
        }

        return list.get(list.size() - 1);
    }

    public void removeAll() {
        list.clear();
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean isFull() {
        return false;
    }

}
