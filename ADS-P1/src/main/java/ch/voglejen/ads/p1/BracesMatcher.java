package ch.voglejen.ads.p1;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class BracesMatcher {

    // Should either use a bidi map or another map if we have a lot of characters
    private Map<String, String> bracesMap = new HashMap<>();

    public void addBrace(char startBrace, char endBrace) {
        bracesMap.put("" + startBrace, "" + endBrace);
    }

    public void addBrace(String startBrace, String endBrace) {
        bracesMap.put(startBrace, endBrace);
    }

    public boolean checkStr(String input) {
        ListStack<String> braceStack = new ListStack<>();
        // Inverse sorted list
        SortedSet<Integer> lengthsToCheck = new TreeSet<>((o1, o2) -> o1.compareTo(o2) * -1);

        for (String key : bracesMap.keySet()) {
            lengthsToCheck.add(key.length());
            lengthsToCheck.add(bracesMap.get(key).length());
        }

        for (int i = 0; i < input.length(); i++) {

            String expected = braceStack.peek();

            if (expected != null && input.startsWith(expected, i)) {
                System.out.println("Close Bracket: " + expected);
                braceStack.pop();
                i += expected.length() - 1;
                continue;
            }

            for (int length : lengthsToCheck) {
                if (length + i > input.length()) {
                    continue;
                }

                String substr = input.substring(i, i + length);
                if (bracesMap.containsKey(substr)) {
                    System.out.println("Open Bracket: " + substr);
                    braceStack.push(bracesMap.get(substr));

                    i += length - 1;
                    break;
                }

                // Malformed close tag
                if (bracesMap.containsValue(substr)) {
                    System.out.println("Invalid Close Brace: " + substr);
                    return false;
                }
            }

        }


        return braceStack.size() == 0;
    }

}
