package ch.voglejen.ads.p1;

public class Divisionable {

    public static int kgV(int a, int b) {
        return (a * b) / ggT(a, b);
    }

    public static int ggT(int a, int b) {
        int r;

        do {
            r = a % b;
            a = b;
            b = r;
        } while (b != 0);

        return a;
    }

}
