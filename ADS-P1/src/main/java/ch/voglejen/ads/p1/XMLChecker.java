package ch.voglejen.ads.p1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ch.voglejen.ads.p1.XMLChecker.XMLToken.*;

public class XMLChecker {

    private String xml;

    private static Pattern commentStartPattern = Pattern.compile("^<!--");
    private static Pattern commentPattern = Pattern.compile("^<!--(.+)-->$");
    private static Pattern shortTagPattern = Pattern.compile("^<([a-zA-Z]+)(\\s.*)*/>$");
    private static Pattern normalTagPattern = Pattern.compile("^<([a-zA-Z])+(\\s.*)?>$");
    private static Pattern normalEndTagPattern = Pattern.compile("^</([a-zA-Z])+(\\s.*)?>$");


    public XMLChecker(String xml) {
        this.xml = xml;
    }

    public boolean checkWellFormed() {

        ListStack<String> tagStack = new ListStack<>();

        try {
            XMLToken nextToken;
            while ((nextToken = getNextToken()) != null) {

                if (nextToken.type == TYPE_OPEN_TAG) {
                    tagStack.push(nextToken.name);
                    System.out.println("PUSH: " + nextToken.name);
                } else if (nextToken.type == TYPE_CLOSE_TAG) {

                    String lastTag = tagStack.pop();
                    System.out.println("POP: " + lastTag);
                    if (!nextToken.name.equals(lastTag)) {
                        return false;
                    }
                }

                // Ignore other tags

            }
        } catch (MalformedXMLException e) {
            System.err.println("Malformed XML: " + e.getMessage());
            e.printStackTrace();
            return false;
        }

        return tagStack.isEmpty();
    }

    private XMLToken getNextToken() throws MalformedXMLException {

        int startPos = xml.indexOf('<');

        if (startPos == -1) {
            return null;
        }


        int endPos = xml.indexOf('>');

        String tag = xml.substring(startPos, endPos + 1);

        // Remove read tag
        xml = xml.substring(endPos + 1);


        if (commentStartPattern.matcher(tag).matches()) {
            Matcher commentMatcher = commentPattern.matcher(tag);

            if (!commentMatcher.matches()) {
                throw new MalformedXMLException(tag);
            }

            return new XMLToken(commentMatcher.group(1), TYPE_COMMENT);

        }

        Matcher shortTagMatcher = shortTagPattern.matcher(tag);
        if (shortTagMatcher.matches()) {
            return new XMLToken(shortTagMatcher.group(1), TYPE_SHORT_TAG);
        }

        Matcher normalTagMatcher = normalTagPattern.matcher(tag);
        if (normalTagMatcher.matches()) {
            return new XMLToken(normalTagMatcher.group(1), TYPE_OPEN_TAG);
        }

        Matcher normalEndTagMatcher = normalEndTagPattern.matcher(tag);
        if (normalEndTagMatcher.matches()) {
            return new XMLToken(normalEndTagMatcher.group(1), TYPE_CLOSE_TAG);
        }

        throw new MalformedXMLException(tag);
    }

    static class XMLToken {

        static final int TYPE_OPEN_TAG = 0;
        static final int TYPE_CLOSE_TAG = 1;
        static final int TYPE_SHORT_TAG = 2;

        static final int TYPE_COMMENT = 3;

        String name;
        int type;

        public XMLToken(String name, int type) {
            this.name = name;
            this.type = type;
        }
    }

    static class MalformedXMLException extends Exception {

        public MalformedXMLException(String tag) {
            super(tag);
        }

    }

}
