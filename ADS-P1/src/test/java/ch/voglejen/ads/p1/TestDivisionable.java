package ch.voglejen.ads.p1;

import junit.framework.TestCase;
import org.junit.Test;

public class TestDivisionable extends TestCase {

    @Test
    public void testGGT() {

        assertEquals(5, Divisionable.ggT(5, 10));
        assertEquals(4, Divisionable.ggT(64, 76));

    }

    public void testKgV() {

        assertEquals(6, Divisionable.kgV(2, 3));

    }

}
