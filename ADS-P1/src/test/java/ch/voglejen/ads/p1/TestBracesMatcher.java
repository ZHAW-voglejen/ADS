package ch.voglejen.ads.p1;

import junit.framework.TestCase;

public class TestBracesMatcher extends TestCase {

    public void testSuccess() {
        BracesMatcher matcher = new BracesMatcher();
        matcher.addBrace('(', ')');
        matcher.addBrace('[', ']');
        matcher.addBrace('{', '}');
        matcher.addBrace("<*", "*>");
        matcher.addBrace("<", ">");


        assertTrue(matcher.checkStr("{ hey + [ wat ] + ( asd { asd [asd ] } )}"));
        assertTrue(matcher.checkStr("<*(<*<>*>)*>"));
        assertFalse(matcher.checkStr("{asd[sd)ds]}"));
        assertFalse(matcher.checkStr("{{{{"));
        assertFalse(matcher.checkStr("}}}}}"));
    }

}
