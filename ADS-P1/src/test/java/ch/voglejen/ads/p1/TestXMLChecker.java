package ch.voglejen.ads.p1;

import junit.framework.TestCase;
import org.junit.Test;

public class TestXMLChecker extends TestCase {

    public void testXML() {
        XMLChecker checker = new XMLChecker("<a>Hello <b /></a>");
        assertTrue(checker.checkWellFormed());

        checker = new XMLChecker("asd asd sad");
        assertTrue(checker.checkWellFormed());

        checker = new XMLChecker("<abc> </sdf>");
        assertFalse(checker.checkWellFormed());

    }

    @Test
    public void testMalformed() {
        XMLChecker checker = new XMLChecker("<aaa lsaddl <a ><abc ");

        assertFalse(checker.checkWellFormed());
    }

}
