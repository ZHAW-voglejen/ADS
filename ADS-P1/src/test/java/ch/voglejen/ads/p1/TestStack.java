package ch.voglejen.ads.p1;

import junit.framework.TestCase;

public class TestStack extends TestCase {

    public void testStackWorks() {

        Stack<String> stack = new Stack<>(String.class, 10);

        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");

        assertEquals("D", stack.pop());
        assertEquals("C", stack.pop());
        assertEquals("B", stack.pop());
        assertEquals("A", stack.pop());


    }

}
